﻿using Microsoft.Extensions.Configuration;

namespace Core.Config
{
    public class Configuration
    {
        private static IConfigurationRoot GetConfiguration
        {
            get
            {
                // var environmentName = Environment.GetEnvironmentVariable("TEST_ENV") ?? "DEV";
                return new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile($"config.DEV.json", true, false)
                    .Build();
            }
        }

        public static BrowserConfig Browser => GetConfiguration.GetSection("Browser").Get<BrowserConfig>();

        public static string GetSetting(string key)
        {
            return GetConfiguration[key];
        }

        public static T GetSettingValue<T>(string key)
        {
            return GetConfiguration.GetValue<T>(key);
        }
    }
}
