﻿using Core.Enum;

namespace Core.Config
{
    public class BrowserConfig
    {
        public string StartUrl { get; set; }
        public bool HeadLess { get; set; }
        public BrowserType Type { get; set; }
        public string Password { get; set; }
        public UserNamesConfig UserNames { get; set; }
    }

    public class UserNamesConfig
    {
        public string StandardUser { get; set; }
        public string LockedOutUser { get; set; }
        public string ProblemUser { get; set; }
        public string PerformanceGlitchUser { get; set; }
        public string ErrorUser { get; set; }
        public string VisualUser { get; set; }
    }
}
