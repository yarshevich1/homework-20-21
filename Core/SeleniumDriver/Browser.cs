﻿using Core.SeleniumDriver.BrowserFactory;
using OpenQA.Selenium;
using System.Reflection;

namespace Core.SeleniumDriver
{
    public class Browser : IDisposable
    {
        protected static readonly ThreadLocal<Browser> BrowserInstances = new (()=> new Browser());
        public IWebDriver WebDriver { get; private set; }
        public static Browser Instance => BrowserInstances.Value; // GetBrowser();

        public Browser()
        {
            WebDriver = DriverFactory.GetDriver();
        }

        private static Browser GetBrowser() => BrowserInstances.Value ?? (BrowserInstances.Value = new Browser());

        public void Navigate(string url)
        {
            WebDriver?.Navigate().GoToUrl(url);
        }

        public void ClearCookie()
        {
            WebDriver?.Manage().Cookies.DeleteAllCookies();
        }

        public void Close()
        {
            WebDriver?.Quit();
            WebDriver?.Dispose();
            BrowserInstances.Value = null;
            WebDriver = null;
        }
        public void Quit()
        {
            WebDriver?.Quit(); // выходим из драйвера
        }

        public void Dispose()
        {
            if (BrowserInstances.IsValueCreated)
            {
                BrowserInstances.Value = null; // Удаляем значение для текущего потока
                WebDriver?.Dispose(); // Освобождаем драйвер
                
            }
        }

        public void ClearLocalStorage()
        {
            ExecuteJavaScript("window.localStorage.clear();");
        }

        public void ClearSessionStorage()
        {
            ExecuteJavaScript("window.sessionStorage.clear();");
        }

        public string GetScreenshot()
        {
            var file = ((ITakesScreenshot)WebDriver).GetScreenshot();
            var img = file.AsBase64EncodedString;

            return img;
        }

        public string SaveScreenshot()
        {
            var fileName = $"{Guid.NewGuid().ToString()}.png";
            var directory = Directory.CreateDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                                                      + "\\screenshots\\").FullName;
            string filePath = directory + fileName;

            var file = ((ITakesScreenshot)WebDriver).GetScreenshot();
            file.SaveAsFile(filePath, ScreenshotImageFormat.Png);

            return filePath;
        }

        private void ExecuteJavaScript(string script)
        {
            ((IJavaScriptExecutor)WebDriver).ExecuteScript(script);
        }

        public void ClearScreenshotsFolder()
        {
            if (Directory.Exists(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                                  + "\\screenshots\\"))
                Directory.Delete(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                                 + "\\screenshots", true);

        }
    }
}
