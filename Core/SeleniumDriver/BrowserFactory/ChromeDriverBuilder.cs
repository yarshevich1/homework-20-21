﻿using Core.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Core.SeleniumDriver.BrowserFactory
{
    internal class ChromeDriverBuilder : DriverBuilder
    {
        public override IWebDriver GetDriver()
        {
            var chromeOptions = new ChromeOptions();

            if (Configuration.Browser.HeadLess)
                chromeOptions.AddArgument("--headless");

            chromeOptions.AddArgument("--no-sandbox");

            var chromeDriver = new ChromeDriver(chromeOptions);

            return chromeDriver;
        }
    }
}
