﻿using Core.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Core.SeleniumDriver.BrowserFactory
{
    internal class FireFoxDriverBuilder : DriverBuilder
    {
        public override IWebDriver GetDriver()
        {
            var firefoxOptions = new FirefoxOptions();

            if (Configuration.Browser.HeadLess)
                firefoxOptions.AddArgument("--headless");

            firefoxOptions.AddArgument("--no-sandbox");

            var firefoxDriver = new FirefoxDriver(firefoxOptions);

            return firefoxDriver;
        }
    }
}
