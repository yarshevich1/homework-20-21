﻿using OpenQA.Selenium;

namespace Core.SeleniumDriver.BrowserFactory
{
    internal abstract class DriverBuilder
    {
        public abstract IWebDriver GetDriver();
    }
}
