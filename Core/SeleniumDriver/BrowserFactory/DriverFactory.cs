﻿using Core.Config;
using Core.Enum;
using OpenQA.Selenium;

namespace Core.SeleniumDriver.BrowserFactory
{
    internal class DriverFactory
    {
        public static readonly Dictionary<BrowserType, DriverBuilder> BrowserDictionary = new()
        {
            {
                BrowserType.Chrome, new ChromeDriverBuilder()
            },
            {
                BrowserType.FireFox, new FireFoxDriverBuilder()
            }
        };

        public static IWebDriver GetDriver()
        {
            return BrowserDictionary[Configuration.Browser.Type].GetDriver();
        }

    }
}
