﻿using Core.Config;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using SauceDemo.Base;
using SauceDemo.PageObjects;

namespace SauceDemo.Tests
{
    [AllureSuite("Login tests")]
    [TestFixture]
    internal class LoginTests : BaseTest
    {
        private LoginPageObject _loginPage;

        [SetUp]
        public void Setup()
        {
            _browser.Navigate(Configuration.Browser.StartUrl);
            _loginPage = new LoginPageObject();

        }

        [Test]
        public void LoginLikeStandardUser()
        {
            var actualResult =_loginPage.Login(Configuration.Browser.UserNames.StandardUser,
                Configuration.Browser.Password)
                .IsLogoVisible();

            Assert.IsTrue(actualResult);
        }

        [Test]
        public void LoginLikeLockedOutUser()
        {
            var actualResult = _loginPage.Login(Configuration.Browser.UserNames.LockedOutUser,
                    Configuration.Browser.Password)
                .IsLocked();

            Assert.IsTrue(actualResult);
        }

        [Test]
        public void LoginLikeProblemUser()
        {
            var actualResult = _loginPage.Login(Configuration.Browser.UserNames.ProblemUser,
                    Configuration.Browser.Password)
                .IsLogoVisible();

            Assert.IsTrue(actualResult);
        }

        [Test]
        public void LoginLikePerformanceGlitchUser()
        {
            var actualResult = _loginPage.Login(Configuration.Browser.UserNames.PerformanceGlitchUser,
                    Configuration.Browser.Password)
                .IsLogoVisible();

            Assert.IsTrue(actualResult);
        }

        [Test]
        public void LoginLikeErrorUser()
        {
            var actualResult = _loginPage.Login(Configuration.Browser.UserNames.ErrorUser,
                    Configuration.Browser.Password)
                .IsLogoVisible();

            Assert.IsTrue(actualResult);
        }

        [Test]
        public void LoginLikeVisualUser()
        {
            var actualResult = _loginPage.Login(Configuration.Browser.UserNames.VisualUser,
                    Configuration.Browser.Password)
                .IsLogoVisible();

            Assert.IsTrue(actualResult);
        }
    }
}
