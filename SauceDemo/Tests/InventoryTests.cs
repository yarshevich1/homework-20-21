﻿using Core.Config;
using NUnit.Allure.Attributes;
using SauceDemo.Base;
using SauceDemo.PageObjects;

namespace SauceDemo.Tests
{
    [AllureSuite("Inventory tests")]
    [TestFixture]
    internal class InventoryTests : BaseTest
    {
        private LoginPageObject _loginPage;

        [SetUp]
        public void Setup()
        {
            _browser.Navigate(Configuration.Browser.StartUrl);
            _browser.ClearCookie();
            _loginPage = new LoginPageObject();

        }

        [Test]
        public void AddToCartThreeItems()
        {
            var actualResult = _loginPage
                .Login(Configuration.Browser.UserNames.StandardUser,
                    Configuration.Browser.Password)
                .AddToCardThreeItems()
                .GoToCart()
                .GetCartItemsCount();

            Assert.AreEqual(3, actualResult);
        }

        [Test]
        public void AddToCartAllItems()
        {
            var actualResult = _loginPage
                .Login(Configuration.Browser.UserNames.StandardUser,
                    Configuration.Browser.Password)
                .AddToCardAllItems()
                .GoToCart()
                .GetCartItemsCount();

            Assert.AreEqual(6, actualResult);
        }

        [Test]
        public void AddToCartBikeLight()
        {
            var actualResult = _loginPage
                .Login(Configuration.Browser.UserNames.StandardUser,
                    Configuration.Browser.Password)
                .AddOrRemoveToCartSauceLabsBikeLight()
                .GoToCart()
                .GetCartItemsCount();

            Assert.AreEqual(1, actualResult);
        }

        [Test]
        public void AddToCartThreeItemsAndGoToCartAndBackToAddOneItems()
        {
            var actualResult = _loginPage
                .Login(Configuration.Browser.UserNames.StandardUser,
                    Configuration.Browser.Password)
                .AddToCardThreeItems()
                .GoToCart()
                .ContinueShoppingButtonClick()
                .AddOrRemoveToCartSauceLabsBikeLight()
                .GoToCart()
                .GetCartItemsCount();

            Assert.AreEqual(4, actualResult);
        }
    }
}
