﻿using Allure.Commons;
using Core.SeleniumDriver;
using ExtentReportLib.Reports;
using NUnit.Allure.Core;
using NUnit.Framework.Interfaces;

namespace SauceDemo.Base
{
    [AllureNUnit]
    [Parallelizable(ParallelScope.Fixtures)]
    internal class BaseTest
    {
        protected Browser _browser;
        private AllureLifecycle _lifecycle;

        [SetUp]
        public void DobeforeEachTest()
        {
            ExtentReporting.CreateTest(TestContext.CurrentContext.Test.MethodName);
        }

        [OneTimeSetUp]
        public void OnTimeSetUp()
        {
            _lifecycle = AllureLifecycle.Instance;
            _browser = Browser.Instance;
            _browser?.ClearScreenshotsFolder();
        }

        [OneTimeTearDown]
        public void OnTimeTearDown()
        {
            _browser?.Close();
           // _browser?.Dispose();
            ExtentReporting.EndReporting();
        }

        [TearDown]
        public void TearDown()
        {
            EndTest();
            EndTestAllure();
            _browser?.ClearLocalStorage();
            _browser?.ClearSessionStorage();
        }

        private void EndTest()
        {
            var testStatus = TestContext.CurrentContext.Result.Outcome.Status;
            var message = TestContext.CurrentContext.Result.Message;

            switch (testStatus)
            {
                case TestStatus.Failed:
                    ExtentReporting.LogFail($"Test has failed {message}");
                    break;
                case TestStatus.Skipped:
                    ExtentReporting.LogInfo($"Test skipped {message}");
                    break;
                default:
                    break;
            }

            ExtentReporting.LogScreenshot("Ending test", _browser.GetScreenshot());

            var screenshot = _browser.SaveScreenshot();
            TestContext.AddTestAttachment(screenshot);
        }

        private void EndTestAllure()
        {
            var screenshot = _browser.SaveScreenshot();
            _lifecycle.AddAttachment("Screenshot","image/png", screenshot);
        }
    }
}
